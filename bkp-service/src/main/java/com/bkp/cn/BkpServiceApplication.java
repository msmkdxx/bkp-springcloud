package com.bkp.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class BkpServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BkpServiceApplication.class, args);
    }

}
