package com.bkp.cn.service.impl;

import com.bkp.cn.dto.NewsDetail;
import com.bkp.cn.dto.NewsDetailExample;
import com.bkp.cn.mapper.NewsDetailMapper;
import com.bkp.cn.service.NewsDetailService;
import com.bkp.cn.vo.NewsDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/258:52
 */
@Service
public class NewsDetailServiceImpl implements NewsDetailService {
    @Autowired
    private NewsDetailMapper newsDetailMapper;

    @Override
    public List<NewsDetail> selectNewsDetail(Long categoryid, String searchStr) {
        NewsDetailExample newsDetailExample = new NewsDetailExample();
        NewsDetailExample.Criteria criteria = newsDetailExample.or();
        if (null != categoryid) {
            criteria.andCategoryidEqualTo(categoryid);
        }
        if (!StringUtils.isEmpty(searchStr)) {
            criteria.andTitleLike('%' + searchStr + '%');
        }
        newsDetailExample.setOrderByClause("id desc");
        List<NewsDetail> newsDetailList = newsDetailMapper.selectByExample(newsDetailExample);
        if (!CollectionUtils.isEmpty(newsDetailList)) {
            return newsDetailList;
        }
        return null;
    }

    @Override
    public NewsDetail selectNewsDetailById(Long id) {
        NewsDetail newsDetail = newsDetailMapper.selectByPrimaryKey(id);
        if (!ObjectUtils.isEmpty(newsDetail)) {
            return newsDetail;
        }
        return null;
    }

    @Override
    public Boolean updateNewsDetail(NewsDetailVo newsDetailVo) {
        NewsDetail newsDetail = new NewsDetail();
        BeanUtils.copyProperties(newsDetailVo, newsDetail);
        return newsDetailMapper.updateByPrimaryKeySelective(newsDetail) > 0;
    }
}
