package com.bkp.cn.service;

import com.bkp.cn.dto.NewsDetail;
import com.bkp.cn.vo.NewsDetailVo;

import java.util.List;

public interface NewsDetailService {
    /**
     * 高级查询新闻列表
     *
     * @param categoryid
     * @param searchStr
     * @return
     */
    List<NewsDetail> selectNewsDetail(Long categoryid, String searchStr);

    /**
     * 查询新闻详情
     *
     * @param id
     * @return
     */
    NewsDetail selectNewsDetailById(Long id);

    /**
     * 修改新闻
     *
     * @param newsDetailVo
     * @return
     */
    Boolean updateNewsDetail(NewsDetailVo newsDetailVo);
}
