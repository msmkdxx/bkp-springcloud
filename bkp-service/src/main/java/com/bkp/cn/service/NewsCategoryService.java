package com.bkp.cn.service;

import com.bkp.cn.dto.NewsCategory;

import java.util.List;

public interface NewsCategoryService {
    /**
     * 查询新闻分类列表
     *
     * @return
     */
    List<NewsCategory> selectNewsCategory();
}
