package com.bkp.cn.service.impl;

import com.bkp.cn.dto.NewsCategory;
import com.bkp.cn.mapper.NewsCategoryMapper;
import com.bkp.cn.service.NewsCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/259:45
 */
@Service
public class NewsCategoryServiceImpl implements NewsCategoryService {
    @Autowired
    private NewsCategoryMapper newsCategoryMapper;

    @Override
    public List<NewsCategory> selectNewsCategory() {
        List<NewsCategory> newsCategoryList = newsCategoryMapper.selectByExample(null);
        if (!CollectionUtils.isEmpty(newsCategoryList)) {
            return newsCategoryList;
        }
        return null;
    }
}
