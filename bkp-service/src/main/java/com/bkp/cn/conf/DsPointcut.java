package com.bkp.cn.conf;

import org.aspectj.lang.annotation.Pointcut;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/4/1618:57
 */
public class DsPointcut {
    @Pointcut("execution(public * com.bkp.cn.service.*.*(..))")
    public void selectPointCut(){

    }
}
