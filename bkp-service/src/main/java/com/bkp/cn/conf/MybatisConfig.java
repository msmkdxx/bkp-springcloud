package com.bkp.cn.conf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.bkp.cn.mapper")
public class MybatisConfig {
}
