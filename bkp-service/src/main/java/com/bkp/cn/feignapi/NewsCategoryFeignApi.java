package com.bkp.cn.feignapi;

import com.bkp.cn.dto.NewsCategory;
import com.bkp.cn.service.NewsCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/259:47
 */
@RestController
@RequestMapping(value = "/newsCategoryFeignApi")
public class NewsCategoryFeignApi {
    @Autowired
    private NewsCategoryService newsCategoryService;

    @GetMapping(value = "/selectNewsCategory")
    public List<NewsCategory> selectNewsCategory(){
        return newsCategoryService.selectNewsCategory();
    }
}
