package com.bkp.cn.feignapi;

import com.bkp.cn.dto.NewsDetail;
import com.bkp.cn.service.NewsDetailService;
import com.bkp.cn.vo.NewsDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/258:53
 */
@RestController
@RequestMapping(value = "/newsFeignApi")
public class NewsDetailFeignApi {
    @Autowired
    private NewsDetailService newsDetailService;

    @GetMapping(value = "/selectNewsDetail")
    public List<NewsDetail> selectNewsDetail(@RequestParam(value = "categoryid", required = false) Long categoryid,
                                             @RequestParam(value = "searchStr", required = false) String searchStr) {
        return newsDetailService.selectNewsDetail(categoryid, searchStr);
    }

    @GetMapping(value = "/selectNewsDetailById")
    public NewsDetail selectNewsDetailById(@RequestParam(value = "id") Long id) {
        return newsDetailService.selectNewsDetailById(id);
    }

    @PostMapping(value = "/updateNewsDetail")
    public Boolean updateNewsDetail(@RequestBody NewsDetailVo newsDetailVo) {
        return newsDetailService.updateNewsDetail(newsDetailVo);
    }
}
