package com.bkp.cn.feign;

import com.bkp.cn.dto.NewsDetail;
import com.bkp.cn.vo.NewsDetailVo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "bkp-service")
public interface NewsDetailFeign {

    @GetMapping(value = "/newsFeignApi/selectNewsDetail")
    List<NewsDetail> selectNewsDetail(@RequestParam(value = "categoryid", required = false) Long categoryid,
                                      @RequestParam(value = "searchStr", required = false) String searchStr);

    @GetMapping(value = "/newsFeignApi/selectNewsDetailById")
    NewsDetail selectNewsDetailById(@RequestParam(value = "id") Long id);

    @PostMapping(value = "/newsFeignApi/updateNewsDetail")
    Boolean updateNewsDetail(@RequestBody NewsDetailVo newsDetailVo);
}
