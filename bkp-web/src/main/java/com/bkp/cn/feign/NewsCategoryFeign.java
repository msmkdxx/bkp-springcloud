package com.bkp.cn.feign;

import com.bkp.cn.dto.NewsCategory;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "bkp-service")
public interface NewsCategoryFeign {

    @GetMapping(value = "/newsCategoryFeignApi/selectNewsCategory")
    public List<NewsCategory> selectNewsCategory();
}
