package com.bkp.cn.controller;

import com.bkp.cn.dto.NewsCategory;
import com.bkp.cn.feign.NewsCategoryFeign;
import com.bkp.cn.utils.ReturnResult;
import com.bkp.cn.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/2510:02
 */
@Api(tags = "新闻分类")
@RestController
@RequestMapping(value = "/newsCategory")
public class NewsCategoryController {
    @Autowired
    private NewsCategoryFeign newsCategoryFeign;

    @ApiOperation(value = "查询新闻分类列表")
    @GetMapping(value = "/selectNewsCategory")
    public ReturnResult<List<NewsCategory>> selectNewsCategory() {
        List<NewsCategory> newsCategoryList = newsCategoryFeign.selectNewsCategory();
        if (CollectionUtils.isEmpty(newsCategoryList)) return ReturnResultUtils.returnFail("暂无分类数据");

        return ReturnResultUtils.returnSucess(newsCategoryList);
    }
}
