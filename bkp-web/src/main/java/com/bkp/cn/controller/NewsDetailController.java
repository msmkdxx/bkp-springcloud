package com.bkp.cn.controller;

import com.bkp.cn.dto.NewsDetail;
import com.bkp.cn.feign.NewsDetailFeign;
import com.bkp.cn.utils.ReturnResult;
import com.bkp.cn.utils.ReturnResultUtils;
import com.bkp.cn.vo.NewsDetailVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/258:56
 */
@Api(tags = "新闻")
@RestController
@RequestMapping(value = "/news")
public class NewsDetailController {
    @Autowired
    private NewsDetailFeign newsDetailFeign;

    @ApiOperation(value = "高级查询新闻列表")
    @GetMapping(value = "/selectNewsDetail")
    public ReturnResult<List<NewsDetail>> selectNewsDetail(@ApiParam(value = "分类id") @RequestParam(value = "categoryid", required = false) Long categoryid,
                                                           @ApiParam(value = "标题模糊搜索") @RequestParam(value = "searchStr", required = false) String searchStr) {
        List<NewsDetail> newsDetailList = newsDetailFeign.selectNewsDetail(categoryid, searchStr);
        if (CollectionUtils.isEmpty(newsDetailList)) return ReturnResultUtils.returnFail("暂无数据");

        return ReturnResultUtils.returnSucess(newsDetailList);
    }

    @ApiOperation(value = "查询新闻详情")
    @GetMapping(value = "/selectNewsDetailById")
    public ReturnResult<NewsDetail> selectNewsDetailById(@ApiParam(value = "新闻编号") @RequestParam(value = "id") Long id) {
        NewsDetail newsDetail = newsDetailFeign.selectNewsDetailById(id);
        if (ObjectUtils.isEmpty(newsDetail)) return ReturnResultUtils.returnFail("查询详情失败！！！");

        return ReturnResultUtils.returnSucess(newsDetail);
    }

    @ApiOperation(value = "修改新闻")
    @PostMapping(value = "/updateNewsDetail")
    public ReturnResult updateNewsDetail(@RequestBody NewsDetailVo newsDetailVo) {
        if (newsDetailFeign.updateNewsDetail(newsDetailVo)) return ReturnResultUtils.returnSucess();

        return ReturnResultUtils.returnFail("修改失败！！！");
    }
}
