/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50045
Source Host           : localhost:3306
Source Database       : news

Target Server Type    : MYSQL
Target Server Version : 50045
File Encoding         : 65001

Date: 2020-05-25 11:32:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for news_category
-- ----------------------------
DROP TABLE IF EXISTS `news_category`;
CREATE TABLE `news_category` (
  `id` bigint(20) NOT NULL auto_increment COMMENT '新闻分类编号',
  `name` varchar(255) NOT NULL COMMENT '新闻分类名称',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_category
-- ----------------------------
INSERT INTO `news_category` VALUES ('1', '军事');
INSERT INTO `news_category` VALUES ('2', '国际');
INSERT INTO `news_category` VALUES ('3', '国内');

-- ----------------------------
-- Table structure for news_detail
-- ----------------------------
DROP TABLE IF EXISTS `news_detail`;
CREATE TABLE `news_detail` (
  `id` bigint(20) NOT NULL auto_increment COMMENT '新闻编号',
  `categoryId` bigint(20) NOT NULL COMMENT '新闻分类编号',
  `title` varchar(255) NOT NULL COMMENT '新闻标题',
  `summary` varchar(255) default NULL COMMENT '新闻摘要',
  `author` varchar(255) default NULL COMMENT '作者',
  `createDate` datetime NOT NULL,
  `updateDate` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_detail
-- ----------------------------
INSERT INTO `news_detail` VALUES ('1', '1', '日本驻华大使返回北京', '日本驻华大使返回北京，使馆证实', '李明', '2020-05-25 08:45:42', '2020-05-25 08:45:49');
INSERT INTO `news_detail` VALUES ('2', '2', '朝鲜劳动党新闻', '朝鲜劳动党到底干了什么', '张三', '2020-05-25 08:46:51', '2020-05-25 10:17:59');
INSERT INTO `news_detail` VALUES ('3', '3', '学生论文', '学生论文造假教师连带责任', '李四', '2020-05-25 08:47:47', '2020-05-25 08:47:51');
