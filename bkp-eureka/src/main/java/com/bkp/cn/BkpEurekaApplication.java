package com.bkp.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class BkpEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(BkpEurekaApplication.class, args);
    }

}
