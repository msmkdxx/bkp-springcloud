package com.bkp.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BkpZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(BkpZuulApplication.class, args);
    }

}
