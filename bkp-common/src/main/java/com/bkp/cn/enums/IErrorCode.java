package com.bkp.cn.enums;

public interface IErrorCode {
    String getErrorCode();
    String getErrorMessage();
}
