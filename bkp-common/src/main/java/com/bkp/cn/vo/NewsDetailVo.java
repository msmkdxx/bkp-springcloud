package com.bkp.cn.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/259:23
 */
@Data
public class NewsDetailVo implements Serializable {
    @ApiModelProperty(value = "新闻编号")
    private Long id;
    @ApiModelProperty(value = "新闻分类编号")
    private Long categoryid;
    @ApiModelProperty(value = "新闻标题")
    private String title;
    @ApiModelProperty(value = "新闻摘要")
    private String summary;
    @ApiModelProperty(value = "作者")
    private String author;
    @ApiModelProperty(value = "创建时间")
    private Date createdate;
    @ApiModelProperty(value = "更新时间")
    private Date updatedate;
}
