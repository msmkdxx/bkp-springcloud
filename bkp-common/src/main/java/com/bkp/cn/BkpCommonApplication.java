package com.bkp.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BkpCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(BkpCommonApplication.class, args);
    }

}
